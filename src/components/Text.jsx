import React from "react"
import marked from "marked"
import htmr from "htmr"

const Text = (props) => {
    const { content } = props

    const markedText = marked(content)
    const htmrText = htmr(markedText)

    return <>{htmrText}</>
}

export default Text
