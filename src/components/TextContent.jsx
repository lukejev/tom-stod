import React from "react"
import Text from "./Text"

const TextContent = (props) => {
    const { children } = props

    return (
        <div className="m-auto w-3/4 max-w-5xl">
            <Text content={children}></Text>
        </div>
    )
}

export default TextContent
