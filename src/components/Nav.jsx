import React from "react"
import { graphql, Link, useStaticQuery } from "gatsby"

const Nav = () => {
    const data = useStaticQuery(graphql`
        query Nav {
            contentfulNav {
                title
                subtitle
            }
        }
    `)

    return (
        <div className="nav__outer mt-6 md:mt-12">
            <div className="nav w-9/12 m-auto">
                <div className="nav__content flex flex-col lg:flex-row justify-between">
                    <Link to="/" aria-label="home page link">
                        <div className="nav__title">
                            <h1 className="nav__main-title text-6xl mb-4">
                                {data.contentfulNav.title}
                            </h1>
                            <h2 className="nav__subtitle text-3xl italic mb-6 tracking-wide">
                                {data.contentfulNav.subtitle}
                            </h2>
                        </div>
                    </Link>
                    <div className="nav__menu">
                        <Link
                            className="text-2xl mr-12 underline md:no-underline md:hover:underline"
                            to="../Work"
                        >
                            Work
                        </Link>
                        <Link
                            className="text-2xl mr-12 md:mr-0 underline md:no-underline md:hover:underline"
                            to="../Contact"
                        >
                            Contact
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Nav
