import React from "react"
import { GatsbyImage } from "gatsby-plugin-image"
import Text from "./Text"

const ImageTextContent = (props) => {
    const { image, alt, text, imgPos, imgLink } = props

    return (
        <div className="image-text-content__outer w-9/12 my-8 mx-auto h-3/4">
            <div
                className={`image-text-content flex flex-col-reverse m-h-4/5 ${
                    imgPos === "Right" ? "md:flex-row-reverse" : "md:flex-row"
                } justify-center`}
            >
                {image !== undefined && (
                    <div className="image-text-content__img-outer md:w-6/12 flex justify-center">
                        <a href={imgLink} target="_blank">
                            <img
                                className="m-auto bg-cover p-4 w-full h-auto max-w-md"
                                src={image}
                                alt={alt}
                            />
                        </a>
                    </div>
                )}
                <div
                    className={`image-text-content__text text-3xl my-auto md:text-2xl ${
                        image !== undefined ? "md:w-6/12" : "md:w-4/5"
                    }`}
                >
                    <Text content={text} />
                </div>
            </div>
        </div>
    )
}

export default ImageTextContent
