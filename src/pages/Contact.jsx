import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import Layout from "../components/Layout"
import ImageTextContent from "../components/ImageTextContent"
import TextContent from "../components/TextContent"

const Contact = () => {
    const data = useStaticQuery(graphql`
        query ContactPage {
            allContentfulContentItem(filter: { page: { eq: "Contact" } }) {
                nodes {
                    contentText {
                        contentText
                    }
                }
            }
        }
    `)
    return (
        <Layout>
            {data.allContentfulContentItem.nodes.map((item, i) => (
                <>
                    <ImageTextContent
                        text={item.contentText.contentText}
                        key={i}
                    />
                </>
            ))}
        </Layout>
    )
}

export default Contact
