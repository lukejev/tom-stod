import * as React from "react"
import { graphql, useStaticQuery } from "gatsby"
import ImageTextContent from "../components/ImageTextContent"
import Layout from "../components/Layout"

const IndexPage = () => {
    const data = useStaticQuery(graphql`
        query HomePage {
            contentfulHomePage {
                homePageText {
                    homePageText
                }
                mainImage {
                    fluid {
                        src
                        sizes
                    }
                    title
                }
            }
        }
    `)

    return (
        <Layout>
            <ImageTextContent
                imgLeft
                text={data.contentfulHomePage.homePageText.homePageText}
                image={data.contentfulHomePage.mainImage.fluid.src}
                alt={data.contentfulHomePage.mainImage.title}
            />
        </Layout>
    )
}

export default IndexPage
