import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import ImageTextContent from "../components/ImageTextContent"
import Layout from "../components/Layout"

const Work = () => {
    const data = useStaticQuery(graphql`
        query WorkPage {
            allContentfulContentItem(filter: { page: { eq: "Work" } }) {
                nodes {
                    contentText {
                        contentText
                    }
                    imagePosition
                    imageLink
                    page
                    contentItemImage {
                        title
                        fluid {
                            src
                            sizes
                        }
                    }
                }
            }
        }
    `)

    return (
        <Layout>
            {data.allContentfulContentItem.nodes.map((item, i) => (
                <ImageTextContent
                    imgPos={item.imagePosition}
                    imgLink={item.imageLink}
                    text={item.contentText.contentText}
                    image={item.contentItemImage?.fluid.src}
                    alt={item.contentItemImage?.title}
                    key={i}
                />
            ))}
        </Layout>
    )
}

export default Work
